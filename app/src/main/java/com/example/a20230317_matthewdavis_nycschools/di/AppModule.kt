package com.example.a20230317_matthewdavis_nycschools.di

import android.provider.SyncStateContract
import com.example.a20230317_matthewdavis_nycschools.common.Constants
import com.example.a20230317_matthewdavis_nycschools.model.remote.SchoolsApi
import com.example.a20230317_matthewdavis_nycschools.model.repository.SchoolsRepoImpl
import com.example.a20230317_matthewdavis_nycschools.model.repository.SchoolsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesSchoolsApi(): SchoolsApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASEURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SchoolsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesSchoolsRepository(
        api: SchoolsApi,
        @IoDispatcher ioDispatcher: CoroutineDispatcher
    ): SchoolsRepository {
        return SchoolsRepoImpl(api, ioDispatcher)
    }

    @Provides
    fun providesIoDispatchers(): CoroutineDispatcher= Dispatchers.IO


}