package com.example.a20230317_matthewdavis_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.di.IoDispatcher
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.model.SATScore
import com.example.a20230317_matthewdavis_nycschools.model.repository.SchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject



/**
 * @author Matthew Davis
 *
 * this viewmodel class holds the business logic
 * using livedata observable pattern to get data from repository
 * and allow UI to access this data ase view only
 *
 * Dependency injection used: Dagger-Hilt
 * @param repository
 *
 *
 */
@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val repository : SchoolsRepository,
    private val ioDispatcher: CoroutineDispatcher
    ) : ViewModel() {


    private var _school = MutableLiveData<HighSchool>()
    val school: LiveData<HighSchool>
        get() = _school

    private var _schoolsLiveData = MutableLiveData<Resource<List<HighSchool>>>()
    val schoolsLiveData: LiveData<Resource<List<HighSchool>>>
        get() = _schoolsLiveData

    private var _scoresLiveData = MutableLiveData<Resource<List<SATScore>>>()
    val scoresLiveData: LiveData<Resource<List<SATScore>>>
        get() = _scoresLiveData



    fun fetchSchools() {
        viewModelScope.launch(ioDispatcher) {
            repository.getSchools().collect(){
                _schoolsLiveData.postValue(it)
            }
        }
    }

    fun fetchScores(dbn: String = getSchool()?.dbn?:"NA") {
        viewModelScope.launch(ioDispatcher) {
            repository.getScores(dbn).collect(){
                _scoresLiveData.postValue(it)
            }
        }
    }

    fun getSchool(): HighSchool? {
        return school.value
    }
    fun setSchool(school:HighSchool){
        _school.value = school
    }

    init {
        fetchSchools()
    }

}