package com.example.a20230317_matthewdavis_nycschools.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.a20230317_matthewdavis_nycschools.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Matthew Davis
 *
 * This MainActivity holds our navigation inside a fragment container view
 *
 * @see com.example.a20230317_matthewdavis_nycschools.ui.SchoolsFragment
 * @See com.example.a20230317_matthewdavis_nycschools.ui.ScoresFragment
 *
 */

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, SchoolsFragment())
            .commit()
    }
}