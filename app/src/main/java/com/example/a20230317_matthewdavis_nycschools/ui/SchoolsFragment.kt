package com.example.a20230317_matthewdavis_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230317_matthewdavis_nycschools.R
import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.ui.adapter.SchoolsAdapter
import com.example.a20230317_matthewdavis_nycschools.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint


/**
 * @author Matthew Davis
 *
 * First fragment
 * Observes the state of the viewmodel and data to display a list of schools
 * Dependency injection used: Dagger-Hilt
 *
 * @see com.example.a20230317_matthewdavis_nycschools.viewmodel.SchoolViewModel
 */
@AndroidEntryPoint
class SchoolsFragment : Fragment() {

    private var _binding: FragmentSchoolsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolViewModel by activityViewModels()

    private val mAdapter by lazy{
        SchoolsAdapter{
            viewModel.setSchool(it)
            showDetail(it.dbn!!)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSchoolsBinding.inflate(inflater,container,false)

        binding.rvSchools.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        viewModel.schoolsLiveData.observe(viewLifecycleOwner){
            when(it){
                is Resource.Loading<*> -> {}
                is Resource.Success<*> -> {
                    initViews(it.data!!)
                }
                is Resource.Error<*> -> {}
            }
        }
        viewModel.fetchSchools()

        return binding.root

    }

    private fun initViews(response: List<HighSchool>) {
        response?.let {
            mAdapter.updateSchools(it)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun showDetail(dbn: String){
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragment_container_view, ScoresFragment.newInstance(dbn))
            .addToBackStack(null)
            .commit()
    }
}