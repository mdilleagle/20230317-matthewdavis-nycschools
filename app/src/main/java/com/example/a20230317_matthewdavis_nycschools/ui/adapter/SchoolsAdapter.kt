package com.example.a20230317_matthewdavis_nycschools.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230317_matthewdavis_nycschools.databinding.SchoolItemBinding
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.viewmodel.SchoolViewModel


class SchoolsAdapter(
    private val schoolsList: MutableList<HighSchool> = mutableListOf(),
    private val clickListener: (HighSchool) -> Unit,
) : RecyclerView.Adapter<SchoolsAdapter.SchoolsViewHolder>() {

    inner class SchoolsViewHolder(val binding: SchoolItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindData(position: Int) {
            binding.tvSchoolName.text = schoolsList[position].schoolName
            binding.tvSchoolAddress.text =
                " ${schoolsList[position].primaryAddressLine1}" +
                        " ${schoolsList[position].city}" +
                        " ${schoolsList[position].stateCode}" +
                        " ${schoolsList[position].zip}"
            binding.cvTop.setOnClickListener {
                Log.d("onclick", "school: ${schoolsList[position]} ")
                clickListener(schoolsList[position])
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SchoolsViewHolder {
        val binding = SchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return schoolsList.size
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.bindData(position)
    }

    fun updateSchools(newSchool: List<HighSchool>){
        schoolsList.clear()
        schoolsList.addAll(newSchool)
        notifyDataSetChanged()
    }
}