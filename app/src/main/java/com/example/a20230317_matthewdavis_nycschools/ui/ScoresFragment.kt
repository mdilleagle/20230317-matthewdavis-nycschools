package com.example.a20230317_matthewdavis_nycschools.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.a20230317_matthewdavis_nycschools.R
import com.example.a20230317_matthewdavis_nycschools.common.Constants.PARAM_SCHOOL_DBN
import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.databinding.FragmentScoreBinding
import com.example.a20230317_matthewdavis_nycschools.model.SATScore
import com.example.a20230317_matthewdavis_nycschools.viewmodel.SchoolViewModel



/**
 * @author Matthew Davis
 *
 * Second fragment
 * Observes the state of the viewmodel and data to display a school and its SAT Scores
 *
 * @see com.example.a20230317_matthewdavis_nycschools.viewmodel.SchoolViewModel
 */
class ScoresFragment : Fragment() {

    private var _binding: FragmentScoreBinding? = null
    private val binding get() = _binding!!
    private val viewModel: SchoolViewModel by activityViewModels()


//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScoreBinding.inflate(inflater,container,false)

        viewModel.scoresLiveData.observe(viewLifecycleOwner){
            when(it){
                is Resource.Loading<*> -> {}
                is Resource.Success<*> -> {
                    initView(it.data!!)
                }
                is Resource.Error<*> -> {}
            }
        }

        viewModel.fetchScores(viewModel.getSchool()?.dbn?:"NA")

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.schoolName?.text =  viewModel.getSchool()?.schoolName ?: "NA"
        _binding?.phone?.text = viewModel.getSchool()?.phoneNumber ?: "NA"
        _binding?.email?.text = viewModel.getSchool()?.schoolEmail ?: "NA"
        _binding?.website?.text = viewModel.getSchool()?.website ?: "NA"
        _binding?.schoolAddress?.text = viewModel.getSchool()?.primaryAddressLine1 ?: "NA"
        _binding?.schoolAddress?.setOnClickListener {
            val lat = viewModel.getSchool()?.latitude
            val lon = viewModel.getSchool()?.longitude
            val gmIntentUri = Uri.parse("google.navigation:q=$lat,$lon")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        }
        _binding?.close?.setOnClickListener {
            fragmentManager?.popBackStack()
        }

    }

    companion object {
        @JvmStatic
        fun newInstance(dbn: String): ScoresFragment{
            val fragment = ScoresFragment()
            val bundle = Bundle()
            bundle.putString(PARAM_SCHOOL_DBN, dbn)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun initView(data: List<SATScore>){
        if(data.isNotEmpty()){
            with(_binding!!){
                mathScore.text = data[0].satMathAvgScore
                criticalReadingScore.text = data[0].satCriticalReadingAvgScore
                writingScore.text = data[0].satWritingAvgScore
                numTaskTaker.text = data[0].numOfSatTestTakers
            }
        } else {
            with(_binding!!){
                mathScore.text = getString(R.string.notfound)
                criticalReadingScore.text = getString(R.string.notfound)
                writingScore.text = getString(R.string.notfound)
                numTaskTaker.text = getString(R.string.notfound)
            }
        }

    }

}