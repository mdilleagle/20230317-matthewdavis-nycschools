package com.example.a20230317_matthewdavis_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolsApp: Application() {
}