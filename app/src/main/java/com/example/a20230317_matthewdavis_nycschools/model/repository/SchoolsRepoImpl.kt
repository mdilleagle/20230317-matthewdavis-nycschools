package com.example.a20230317_matthewdavis_nycschools.model.repository

import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.di.IoDispatcher
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.model.SATScore
import com.example.a20230317_matthewdavis_nycschools.model.remote.SchoolsApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.Response
import javax.inject.Inject

class SchoolsRepoImpl @Inject constructor(
    private val api: SchoolsApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
): SchoolsRepository {

    override suspend fun getSchools(): Flow<Resource<List<HighSchool>>> = flow {
        emit(Resource.Loading())
        try {
            val response = api.getSchools()
            if (response.isSuccessful){
                response.body()?.let {
                    emit(Resource.Success(it))
                }?: throw Exception("Info not available")
            } else {
                throw Exception(response.errorBody().toString())
            }
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage))
        }
    }

    override suspend fun getScores(dbn: String): Flow<Resource<List<SATScore>>> = flow{
        emit(Resource.Loading())
        try {
            val response = api.getScores(dbn)
            if (response.isSuccessful){
                response.body()?.let {
                    emit(Resource.Success(it))
                }?:throw Exception("Info not available")
            } else {
                throw Exception(response.errorBody().toString())
            }
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage))
        }
    }
}