package com.example.a20230317_matthewdavis_nycschools.model.remote

import com.example.a20230317_matthewdavis_nycschools.common.Constants
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.model.SATScore
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {

    @GET(Constants.NYC_SCHOOLS_ENDPOINT)
    suspend fun getSchools(): Response<List<HighSchool>>

    @GET(Constants.NYC_SCORES_ENDPOINT)
    suspend fun getScores(
        @Query("dbn") dbn: String
    ): Response<List<SATScore>>
}