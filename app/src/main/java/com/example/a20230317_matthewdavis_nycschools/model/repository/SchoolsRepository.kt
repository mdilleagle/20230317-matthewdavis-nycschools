package com.example.a20230317_matthewdavis_nycschools.model.repository

import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.model.SATScore
import kotlinx.coroutines.flow.Flow

interface SchoolsRepository {

    /**
     * This method will retrieve all the schools available
     *
     * @return The list of schools [HighSchool]
     */

    suspend fun getSchools(): Flow<Resource<List<HighSchool>>>

    /**
     * This method will retrieve the SAT scores for an specific school
     *
     * @param dbn - The unique identifier for each school
     *
     * @return THe list of [SATScore] available for the given school dbn
     */
    suspend fun getScores(dbn: String): Flow<Resource<List<SATScore>>>
}