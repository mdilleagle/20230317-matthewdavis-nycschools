package com.example.a20230317_matthewdavis_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.a20230317_matthewdavis_nycschools.common.Resource
import com.example.a20230317_matthewdavis_nycschools.model.HighSchool
import com.example.a20230317_matthewdavis_nycschools.model.repository.SchoolsRepository
import com.example.a20230317_matthewdavis_nycschools.util.MainCoroutineRule
import io.mockk.*
import io.mockk.MockKSettings.relaxed
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class SchoolViewModelTest {


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: SchoolViewModel
    private val mockRepository = mockk<SchoolsRepository>(relaxed = true)
    private val testDispatcher = UnconfinedTestDispatcher()


    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = SchoolViewModel(mockRepository,testDispatcher)
    }

    @After
    fun tearDown() {
        clearAllMocks()
        Dispatchers.resetMain()
    }

    @Test
    fun `get schools when repository gets an error and returns an error state`() {
        //triple AAA
        // Assignment, Action, Assertion

        coEvery { mockRepository.getSchools() } returns flowOf (
            Resource.Error("error")
        )


        val testObject = SchoolViewModel(mockRepository, testDispatcher)

        testObject.schoolsLiveData.observeForever{
            if (it is Resource.Error) {
                assertEquals(it.message, "error")
            }
        }
    }
    @Test
    fun `get schools when repository gets data and returns a success state`() {

        coEvery { mockRepository.getSchools() } returns flowOf (
            Resource.Success(listOf(mockk(), mockk()))
        )


        val testObject = SchoolViewModel(mockRepository, testDispatcher)

        testObject.schoolsLiveData.observeForever{
            if (it is Resource.Success) {
                assertEquals(it.data?.size, 2)
            }
        }
    }
    @Test
    fun `get scores when repository gets an error and returns an error state`() {

        coEvery { mockRepository.getScores("111") } returns flowOf (
            Resource.Error("error")
        )

        val testObject = SchoolViewModel(mockRepository, testDispatcher)

        testObject.schoolsLiveData.observeForever{
            if (it is Resource.Error) {
                assertEquals(it.message, "error")
            }
        }
    }
    @Test
    fun `get scores when repository gets data and returns a success state`() {

        coEvery { mockRepository.getScores("111") } returns flowOf (
            Resource.Success(listOf(mockk(), mockk()))
        )

        val testObject = SchoolViewModel(mockRepository, testDispatcher)

        testObject.schoolsLiveData.observeForever{
            if (it is Resource.Success) {
                assertEquals(it.data?.size, 2)
            }
        }
    }
}