# 20230317-MatthewDavis-NYCSchools


## Name
NYC Schools coding assesment

## Description
This app will get the list of highschools from the API provided. User can click on the school to see SAT scores for that school. Using MVVM Archiecture

## Libraries
dagger-hilt, retrofit, coroutines, junit, mockito, mockk


